//
//  ViewController.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 3.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Moya
import Kingfisher
import SwiftSoup

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate{
 
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var tableView: UITableView!
    
    let scoresfixtures: ScoresFixturesViewController = ScoresFixturesViewController()
    
    @IBOutlet weak var segmented: UISegmentedControl!
    
    var items = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webViewLoad()
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        userProvider.request(.getAllData) { (result) in
            switch result{
            case .success(let response):
                print("response=\(response)")
                let items = try! JSONDecoder().decode(ItemsResponse.self, from: response.data)
                self.items = items.items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
                }
                
                
            case .failure(let error):
                ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
                print(error)
            }
        }
        

    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       if let cell: MainTableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! MainTableViewCell{
        
        
        
        do {
            let html: String = self.items[indexPath.row].content
            let doc: Document = try SwiftSoup.parse(html)
            let link: Element = try doc.select("a").first()!

            let linkHref: String = try link.attr("href"); // "http://example.com/"
            cell.imgMain.kf.setImage(with: URL(string: "\(linkHref)"))
        } catch Exception.Error(let type, let message) {
            print(message)
        } catch {
            print("error")
        }
        
        var urlString = "\(self.items[indexPath.row].author.image.url)"
        let urlAuthor = URL(string: "https:\(urlString)")
            cell.imgAuthor.kf.setImage(with: urlAuthor)
        
        
        cell.lblTitle.text = items[indexPath.row].title
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let newsDetailsViewController: NewsDetailsViewController = PDVAPI_PageManager.getStoryboardWithIdentifierWithStoryboardId("Main", "NewsDetailsViewController") as! NewsDetailsViewController{
            
            newsDetailsViewController.news = items[indexPath.row]
            self.present(newsDetailsViewController, animated: true)
        }
    }
    
    
    @IBAction func action_segmented(_ sender: Any) {
        if segmented.selectedSegmentIndex == 1{
            print("segment 1")
            
        }else{
            print("segment 0")
        }
    }
    
    @IBAction func action_share(_ sender: Any) {
        let titleName = "Akigolar mobil uygulamasını çok beğendim, haydi sen de yükle! :)"
        let desc = "www.akigolar.com"
        let urlItem : NSURL = NSURL(string: "https//:www.akigolar.com")!
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [titleName, desc, urlItem], applicationActivities: nil)
        //Paylaşılmak istenen veriler activityItems'te belirtilir.
        // Paylaş ekranında çıkmasını istemediğiniz uygulamaları buraya ekleyerek exculude edebilirsiniz.
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    func webViewLoad(){
        
        webView.delegate = self
        let url = URL(string: "http://haberciniz.biz/service/sport/league_standing_table.php?color=1&select=TUR1,TUR2,SPA1,ENG1,GER1%20HTTP/1.1")
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
    }
    
    
    
}

