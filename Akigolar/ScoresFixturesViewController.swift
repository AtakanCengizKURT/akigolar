//
//  ScoresFixturesViewController.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 3.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ScoresFixturesViewController: UIViewController, UIWebViewDelegate {

    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
      webViewLoad()
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
//        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)

    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    func webViewLoad(){
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        
        webView.delegate = self
        let url = URL(string: "http://haberciniz.biz/service/sport/league_standing_table.php?color=1&select=TUR1,TUR2,SPA1,ENG1,GER1%20HTTP/1.1")
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
