//
//  TeamViewController.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 5.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Firebase

class TeamViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    
    var ref: DatabaseReference!
    var team = [AnyObject]()
    var teamDetail: AnyObject?

    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        
        ref = Database.database().reference()
        ref.child("Team").observe(DataEventType.value, with: { (snapshot) in
            let value = snapshot.value as? [AnyObject]

            if let value = value{
                self.team = value
                
                self.ref.child("TeamDetail").observe(.value) { (snapshot) in
                    let value = snapshot.value as? AnyObject
                    if let value = value{
                        self.teamDetail = value
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
            
            
            
            ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1 + self.team.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            if let cell: TecnicalStaffTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TecnicalStaffTableViewCell{

                
                
                return cell
            }
        }else{
        
        if let cell: TeamTableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! TeamTableViewCell{
            cell.imgPlayer.image = UIImage(named: "akhisarspor")
            cell.lblPlayerBirthdayAge.text = "1 Ocak 2000 (18)"
            cell.lblPlayerName.text = "Halil YERAL"
            cell.lblPlayerHeight.text = "1.90 m"
            cell.lblPlayerPosition.text = "Kaleci"
            
            return cell
            }}
        
        
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
          return 300
        }else{
        return 122
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
