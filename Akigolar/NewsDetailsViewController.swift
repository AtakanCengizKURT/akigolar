//
//  NewsDetailsViewController.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 29.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSoup

class NewsDetailsViewController: UIViewController {

    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    
    
    var news: Item? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard news != nil else{
            self.dismiss(animated: true, completion: nil)
            return}
        
        
        
        self.lblTitle.text = news!.title
        

        
        do {
            let html: String = news!.content
            let doc: Document = try SwiftSoup.parse(html)
            let link: Element = try doc.select("a").first()!
            
//            "<p>An <a href='http://example.com/'><b>example</b></a> link.</p>"
//            let text: String = try doc.body()!.text(); // "An example link"
            let linkHref: String = try link.attr("href"); // "http://example.com/"
//            let linkText: String = try link.text(); // "example""
            self.imgNews.kf.setImage(with: URL(string: "\(linkHref)"))
//            let linkOuterH: String = try link.outerHtml(); // "<a href="http://example.com"><b>example</b></a>"
//            let linkInnerH: String = try link.html(); // "<b>example</b>"
            
            let text = try doc.body()!.text()
            self.lblDetails.text = text
            
        } catch Exception.Error(let type, let message) {
            print(message)
        } catch {
            print("error")
        }
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func action_close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func action_share(_ sender: Any) {
        let titleName = """
        \(news!.title)
        Akigolar mobil uygulamasını çok beğendim, haydi sen de yükle! :)
        """
        let desc = "www.akigolar.com"
        let urlItem  = URL(string: "https//:www.akigolar.com")!
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [titleName, desc, urlItem], applicationActivities: nil)
        //Paylaşılmak istenen veriler activityItems'te belirtilir.
        // Paylaş ekranında çıkmasını istemediğiniz uygulamaları buraya ekleyerek exculude edebilirsiniz.
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
