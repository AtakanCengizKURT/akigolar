//
//  MainTableViewCell.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 3.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var imgAuthor: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
