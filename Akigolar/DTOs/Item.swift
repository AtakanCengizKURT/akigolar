//
//  Item.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 29.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation


public struct Item: Decodable{
    public let published: String
    public let url: String
    public let title: String
    public let content: String
    public let author: Author
}


public struct Author: Decodable{
    public let displayName: String
    public let url: String
    public let image: urlImage
}

public struct urlImage: Decodable{
    public let url: String
}
