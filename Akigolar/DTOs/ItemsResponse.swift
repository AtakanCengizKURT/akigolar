//
//  ItemsResponse.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 29.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation


public struct ItemsResponse: Decodable{
    enum CodingKeys: String, CodingKey{
        case items = "items"
    }
    
    public let items: [Item]
    
    
    
}
