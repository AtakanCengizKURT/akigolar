//
//  AnnouncementsViewController.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 3.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher


class AnnouncementsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
    
 
    @IBOutlet weak var tableView: UITableView!
    var ref: DatabaseReference!
    var announcements = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        ref = Database.database().reference()
        
        ref.child("Announcements").observe(DataEventType.value, with: { (snapshot) in
            let value = snapshot.value as? [AnyObject]
//            value![0]["image"]
            
            if let value = value{
                self.announcements = value
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            
            ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
            
        }) { (error) in
            print(error.localizedDescription)
        }
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return announcements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell:AnnouncementsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AnnouncementsTableViewCell{
            cell.lblTitle.text = self.announcements[indexPath.row]["announcement"] as? String
            cell.lblDetail.text = self.announcements[indexPath.row]["detail"] as? String
            
            if self.announcements[indexPath.row]["image"] as? String != "" && self.announcements[indexPath.row]["image"] != nil {
            cell.img.kf.setImage(with:  URL(string: "\(self.announcements[indexPath.row]["image"] as! String )"))
            }else{
                cell.img.image = UIImage(named: "akhisarspor")
            }
        return cell
        }
    }
    
    
    
    @IBAction func action_close(_ sender: Any) {
navigationController?.popToRootViewController(animated: true)    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
