//
//  PDVAPI_PageManager.swift
//  SarjIstanbul
//
//  Created by Atakan Cengiz KURT on 13/09/2018.
//  Copyright © 2017 merena. All rights reserved.
//

import UIKit

class PDVAPI_PageManager: NSObject {
    
    class func getStoryboardWithIdentifier(_ storyboardName: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        return viewController
    }
    
    
    class func getStoryboardWithIdentifierWithStoryboardId(_ storyboardName: String, _ storyboardId: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        return viewController
        
        
    }
    
    class func goToStoryboardWithStoryboardName(_ storyboardName: String) {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = storyboard.instantiateInitialViewController()
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve, animations: {
            appDelegate.window!.rootViewController = viewController;
        }, completion: { finish in
            
        })
        
        
    }
    
    class func goToStoryboardWithStoryboardName(_ s: String, _ s1: String) {
        
        let storyboard = UIStoryboard(name: s, bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = storyboard.instantiateViewController(withIdentifier: s1)
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve, animations: {
            appDelegate.window!.rootViewController = viewController;
        }, completion: { finish in
            
        })
        
    }
}
