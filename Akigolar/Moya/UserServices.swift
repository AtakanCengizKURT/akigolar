//
//  UserServices.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 28.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation
import Moya

let blogId = "2843884778800605975"
let key = "AIzaSyAUerr4Lsms-EUAoM3eY1L3jtc5xELCmAQ"

enum UserServices{
    case getAllData
}

extension UserServices: TargetType{
    var baseURL: URL {
        return URL(string: "https://www.googleapis.com/blogger/v3/blogs/\(blogId)/posts?key=\(key)")!
    }
    
    var path: String {
        switch self {
        case .getAllData:
            return ""
       
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllData:
            return .get
        
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getAllData:
            return .requestPlain
        
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
