//
//  TeamTableViewCell.swift
//  Akigolar
//
//  Created by Atakan Cengiz KURT on 5.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblPlayerNumber: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
   
    @IBOutlet weak var lblPlayerBirthdayAge: UILabel!
    @IBOutlet weak var lblPlayerHeight: UILabel!
    @IBOutlet weak var lblPlayerPosition: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
